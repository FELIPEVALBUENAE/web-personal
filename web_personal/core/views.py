from django.shortcuts import render
# Create your views here.
def Index(request):
    return render(request,"core/index.html")

def Contact(request):
    return render(request,"core/contact.html")

def About(request):
    return render(request,"core/about.html")